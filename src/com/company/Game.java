package com.company;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;
import java.util.Scanner;
import java.util.Random;

public class Game {
    Random random = new Random();
    Scanner in = new Scanner(System.in);


    int reserveNum; //Диапазон
    int num; // Кол-во попыток
    int release;
    int remember;
    String attempts = "You have " + num + " attempts.";

    public Game() throws IOException {
    }

    public void setNumber() throws IOException {
        if (true) {
            Process cmd = Runtime.getRuntime().exec("leafpad input.txt");
            System.out.println("Set data! \n'Ok' - to save, and start.");
            String save = in.next();
            if (save.equals("Ok") || save.equals("ok")) {
                System.out.println("+")
                ;
            } else {
                System.out.println("-");
                return;
            }
        }
        List<String> lines = Files.readAllLines(Paths.get("input.txt"), Charset.defaultCharset());
        reserveNum = (Integer.parseInt(lines.get(1)));
        num = Integer.parseInt(lines.get(3));
        attempts = "You have " + num + " attempts.";
        System.out.println("Hi, I made a random number, try to guess.");
        do {
            try {
                int result = num - 1;
                if (result < 0) {
                    System.out.print("Sorry, you did not guess the number :(");
                    return;
                }
                num -= 1;
                System.out.println(attempts);
                System.out.print("Number: ");
                int inputNum = in.nextInt();
                if (inputNum == reserveNum) {
                    System.out.println("Well done, you guessed right :)");
                    return;
                } else {
                    attempts = "You have " + String.valueOf(result) + " attempts.";
                }
                if (inputNum < reserveNum) {
                    release = reserveNum - inputNum;
                    if (remember > release) {
                        if (result != 4) {
                            System.out.println("Warm\n");
                        }
                    } else if (remember == release) {
                        if (result != 4) {
                            System.out.println("Equals\n");
                        }
                    } else if (result != 4) {
                        System.out.println("Cold\n");
                    }
                    remember = release;
                } else {
                    release = reserveNum + inputNum;
                    if (remember > release) {
                        if (result != 4) {
                            System.out.println("Warm\n");
                        }
                    } else if (remember == release) {
                        if (result != 4) {
                            System.out.println("Equals\n");
                        }
                    } else if (result != 4) {
                        System.out.println("Cold\n");
                    }
                    remember = release;
                }
            } catch (Exception e) {
                System.out.print("Invalid input.");
                return;
            }
        } while (true);

    }
}
