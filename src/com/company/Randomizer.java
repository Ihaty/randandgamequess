package com.company;

import java.util.Scanner;
import java.util.Random;

public class Randomizer {
    Random random = new Random();
    Scanner in = new Scanner(System.in);

    public Randomizer() {
    }

    public void setRandom() {
        int[] dataBaseRandom = new int[1000];
        System.out.print("Write a 'start' to start or 'exit' to exit.\n");
        String getStartOrExit = in.next();
        if (getStartOrExit.equals("start") || getStartOrExit.equals("Start")) {
            do {
                try {
                    System.out.print("Please input min value: ");
                    String minValue = in.next();
                    if (minValue.equals("exit") || minValue.equals("Exit")) {
                        System.out.println("Goodbye :)\n");
                        return;
                    }
                    System.out.print("Please input max value: ");
                    String maxValue = in.next();
                    if (maxValue.equals("exit") || maxValue.equals("Exit")) {
                        System.out.println("Goodbye :)\n");
                        return;
                    }
                    int result = random.nextInt(Integer.parseInt(maxValue));
                    if (Integer.parseInt(minValue) >= Integer.parseInt(maxValue)) {
                        System.out.println("The minimum value cannot exceed the maximum");
                    } else {
                        if (result >= Integer.parseInt(minValue)) {
                            System.out.println("Your random number: " + result);
                        } else {
                            for (int i = 0; result <= Integer.parseInt(minValue); i++) {
                                result += i;
                            }
                            System.out.println("Your random number: " + result);
                        }
                        for (int i = 0; dataBaseRandom.length > i; i++) {
                            if (dataBaseRandom[i] == result) {
                                break;
                            } else {
                                if (dataBaseRandom[i] == 0) {
                                    dataBaseRandom[i] = result;
                                    break;
                                }
                            }
                        }
                    }
                } catch (NumberFormatException e) {
                    System.out.println("Invalid input!");
                }
            } while (true);
        } else if (getStartOrExit.equals("exit") || getStartOrExit.equals("Exit")) {
            System.out.println("Goodbye :)\n");
            return;
        } else {
            System.out.println("Invalid input!");
            setRandom();
        }
    }
}
